<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('halaman.daftar');
    }
    public function kirim(Request $request)
    {
        $fname = $request['fnama'];
        $lname = $request['lnama'];
        $gender = $request['jnsklmn'];
        $nation = $request['negara'];
        $lang1 = $request['lang'];


        return view('halaman.selamat', compact('fname', 'lname', 'gender', 'nation', 'lang1'));
    }
}
